== Extension permanent link
Permanent link: http://shop.etwebsolutions.com/eng/et-tools.html
Support link: http://support.etwebsolutions.com/projects/et-tools/roadmap

== Short Description
Создавая новые сайты, постоянно сталкиваемся с необходимостью
сделать небольшие доработки в коде Magento.
Так как делать отдельный модуль под каждое изменение нецелесообразно
и добавлять в проект кучу модулей не удобно, то и был создан этот модуль.


Доработки:

1. переписан блок Mage_Customer_Block_Account_Navigation.

Добавлена функция removeLinkByName() позволяющая удалять ссылки из меню учётной записи.
Примеры:
* Billing Agreements
* Recurring Profiles
    <customer_account translate="label">
        <reference name="customer_account_navigation">
            <action method="removeLinkByName"><name>billing_agreements</name></action>
            <action method="removeLinkByName"><name>recurring_profiles</name></action>
            <action method="removeLinkByName"><name>tags</name></action>
            <action method="removeLinkByName"><name>wishlist</name></action>
            <action method="removeLinkByName"><name>OAuth Customer Tokens</name></action>
            <action method="removeLinkByName"><name>downloadable_products</name></action>
        </reference>
    </customer_account>

2.



== Version Compatibility
Magento CE:
1.9.x (tested in 1.9.0.1)


== Installation
* Disable compilation if it is enabled (System -> Tools -> Compilation)
* Disable cache if it is enabled (System -> Cache Management)
* Download the extension or install the extension from Magento Connect
* If you have downloaded it, copy all files from the "install" folder to the Magento root folder - where your index.php is
* Log out from the admin panel
* Log in to the admin panel with your login and password
* Set extension's parameters (System -> Configuration -> ET EXTENSIONS -> Tools)
* Run the compilation process and enable cache if needed

